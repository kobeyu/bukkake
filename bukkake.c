#include "runtime.h"
#include "barrier.h"
#include "threadpool.h"
#include <fcntl.h>
#include "stdio.h"
#include "errno.h"
#include "unistd.h"
#include "stdlib.h"
#include "sys/stat.h"
#include "sys/mman.h"

#define DATA_SIZE 100000

uint8_t* bu_data; // FIXME this is global for gdb but no other reason
struct barrier** barriers;
unsigned long** line_barriers;
unsigned long* line_barrier_indices;
func* lines;
struct threadpool* pool;

int ignore(char c) {
    switch(c) {
        case '+':
        case '-':
        case '*':
        case '\n':
        case '|':
        case '.':
        case ',':
        case '>':
        case '<':
        case ']':
        case '[':
                    return 0;
        default:
                    return 1;
    }
}

int main(int argc, char* argv[]) {
    bu_data = (uint8_t*) calloc(DATA_SIZE, sizeof(uint8_t*));
    int i;
    for (i = 1; i < argc; i++) {
        // load file
        char* filename = argv[i];
        int fd = open(filename, O_RDONLY);
        if (fd == -1) {
            perror(filename);
            exit(errno);
        }
        struct stat filestatus;
        if (fstat(fd, &filestatus) == -1) {
            perror(filename);
            exit(errno);
        }
        char* exec = NULL;
        if (filestatus.st_size) {
            exec = mmap(exec, filestatus.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
            if (exec == MAP_FAILED) {
                perror(filename);
                exit(errno);
            }

            // find longest column
            long column = 0;
            unsigned long longest_row = 0;
            unsigned long num_rows = 0;
            int has_spawns = 0;
            size_t pc;
            for (pc = 0; pc < filestatus.st_size; pc++) {
                if (exec[pc] == '\n') {
                    num_rows++;
                    column = 0;
                    continue;
                } else if (num_rows == 0 && exec[pc] == '*') {
                    has_spawns = 1;
                }
                column += 1;
                if (column > longest_row) {
                    longest_row = column;
                }
            }

            barriers = calloc(longest_row, sizeof(struct barrier*));
            line_barriers = (unsigned long**) calloc(num_rows, sizeof(unsigned long*));
            line_barrier_indices = (unsigned long*) calloc(num_rows, sizeof(unsigned long));
            lines = calloc(num_rows + 1, sizeof(func));

            // jit
            unsigned int line_number = 0;
            lines[line_number] = bu_init(line_number);
            column = -1;
            for (pc = 0; pc < filestatus.st_size; pc++) {
                column += 1;
                int8_t offset;
                int32_t traversal;
                switch(exec[pc]) {
                    case '+': 
                        offset = 1;
                        while ((pc + 1 < filestatus.st_size && (exec[pc + 1] == '+')) || ignore(exec[pc + 1])) {
                            if (!ignore(exec[pc + 1])) {
                                offset++;
                            }
                            pc++;
                            column++;
                        }
                        if (offset == 1) {
                            bu_plus(has_spawns);
                        } else {
                            bu_add(offset, has_spawns);
                        }
                        break;
                    case '-':
                        offset = -1;
                        while ((pc + 1 < filestatus.st_size && (exec[pc + 1] == '-')) || ignore(exec[pc + 1])) {
                            if (!ignore(exec[pc + 1])) {
                                offset--;
                            }
                            pc++;
                            column++;
                        }
                        if (offset == -1) {
                            bu_minus(has_spawns);
                        } else {
                            bu_add(offset, has_spawns);
                        }
                        break;
                    case '>':
                    case '<':
                        traversal = 0;
                        offset = 0; // use as continue var
                        while (pc < filestatus.st_size) {
                            switch (exec[pc]) {
                                case '>':
                                    traversal++;
                                    offset = 1;
                                    break;
                                case '<':
                                    traversal--;
                                    offset = 1;
                                    break;
                                default:
                                    break;
                            }
                            if (offset || ignore(exec[pc])) {
                                column++;
                                pc++;
                                offset = 0;
                            } else {
                                break;
                            }
                        }
                        column--;
                        pc--;
                        if (traversal == 1) {
                            bu_right();
                        } else if (traversal == -1) {
                            bu_left();
                        } else if (traversal < 127 || traversal > -128) {
                            bu_chg8(traversal);
                        } else {
                            bu_chg32(traversal);
                        }
                        break;
                    case '[':
                        while (pc + 1 < filestatus.st_size && ignore(exec[pc + 1])) {
                            column++;
                            pc++;
                        }
                        if (pc + 1 < filestatus.st_size && exec[pc + 1] == ']') {
                            bu_waitzero();
                            pc++;
                            column++;
                        } else {
                            bu_loop();
                        }
                        break;
                    case ']':
                        bu_end(filename, line_number + 1);
                        break;
                    case '.':
                        bu_put();
                        break;
                    case ',':
                        bu_get();
                        break;
                    case '\n':
                        bu_close(filename, line_number + 1);
                        line_number += 1;
                        lines[line_number] = bu_init(line_number);
                        column = -1;
                        break;
                    case '*':
                        bu_clone();
                        break;
                    case '|':
                        if (barriers[column] == NULL) {
                            barriers[column] = barrier_new();
                        }
                        if (line_barriers[line_number] == NULL) {
                            line_barriers[line_number] = (unsigned long*) calloc(longest_row, sizeof(unsigned long));
                        }
                        line_barriers[line_number][line_barrier_indices[line_number]] = column;
                        line_barrier_indices[line_number]++;
                        bu_block(barriers[column]);
                        break;
                }
            }
            bu_close(filename, line_number + 1);

            /* DEBUG
            for(uint8_t* prog = (uint8_t*) enter; *prog != 0xc3; prog++) {
                printf("%x\t", *prog);
            }
            printf("c3\n");
            */

            if (munmap(exec, filestatus.st_size)) {
                perror(filename);
                exit(errno);
            }
            if (close(fd)) {
                perror(filename);
                exit(errno);
            }

            pool = threadpool_new();

            bu_enter(0);
            lines[0](bu_data + DATA_SIZE / 2);
            threadpool_joinall(pool);
            threadpool_delete(pool);
            size_t j;
            for (j = 0; j < num_rows; j++) {
                bu_free(lines[j]);
            }
            free(lines);
            for (j = 0; j < longest_row; j++) {
                if (barriers[j]) {
                    barrier_delete(barriers[j]);
                }
            }
            free(barriers);
            // TODO free their members
            for (j = 0; j < num_rows; j++) {
                if (line_barriers[j]) {
                    free(line_barriers[j]);
                }
            }
            free(line_barriers);
            free(line_barrier_indices);
        } else {
            if(close(fd)) {
                perror(filename);
                exit(errno);
            }
            // empty file
        }
    }
    free(bu_data);
    return EXIT_SUCCESS;
}
