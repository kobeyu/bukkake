Bukkake
=======

A parallel brainfuck JIT in C. Design of Brainkkake was cooperative
between Kathy Spradlin, Christopher de la Iglesia, and William Morriss, who
were originally implementing it as a Scala DSL with Colin Walker. Chris has
additionally implemented it in Go.

[Scala DSL](https://github.com/cmdli/parallel-brainfuck)

[Go](https://github.com/cmdli/gokkake)

Modifications to brainfuck:
---------------------------

- __\n__ : _return_; exit thread and denote beginning of another function

- __\*__  : _relative spawn_; spawn a thread running the line number that is
the sum of your line number and the current byte

- __|__  : _barrier_; block until all threads with a barrier in this column are at said
column

Notes:
------

- Memory is shared.

- Spawned threads copy their parent's spawn-time data pointer.

- Increments and decrements are atomic.

Examples:
---------

One of the features of this language is that it has a two-character forkbomb.

    bomb.bk
    **

Another benefit is the ease of deadlock: 3 characters.

    deadlock.bk
    |*|

Known Issues:
-------------

- Portability currently limited to POSIX-compliant x64 systems. It is confirmed
to work on Linux and Mac OS X.
