#include "stdlib.h"
#include "stdint.h"
#include "stack.h"
#define CAPACITY 10000
struct stack {
    size_t size;
    uint8_t* elems[CAPACITY];
};
void stack_init(struct stack* this) {
    this->size = 0;
}
struct stack* stack_new() {
    struct stack* ptr = malloc(sizeof(struct stack));
    stack_init(ptr);
    return ptr;
}
uint8_t* stack_pop(struct stack* this) {
    return this->elems[--(this->size)];
}
void stack_push(struct stack* this, uint8_t* data) {
    this->elems[this->size++] = data;
}
int stack_empty(struct stack* this) {
    return this->size == 0;
}
