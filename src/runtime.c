#include "jit.h"
#include "stack.h"
#include "runtime.h"
#include "barrier.h"
#include "threadpool.h"
#include "stdlib.h"
#include "stdio.h"
#include "pthread.h"
#include "sched.h"

#ifdef __GLIBC__
#include "error.h"
#endif

struct stack* bu_loops;

extern struct barrier** barriers;
extern unsigned long** line_barriers;
extern unsigned long* line_barrier_indices;
extern func* lines;
extern struct threadpool* pool;

void bu_enter(uint64_t line_number) {
    int i;
    for (i = 0; i < line_barrier_indices[line_number]; i++) {
        barrier_register(barriers[line_barriers[line_number][i]]);
    }
}
func bu_init(uint64_t line_number) {
    bu_loops = stack_new();
    func ret = jit_init(NULL);

    // get rcx from param
    // mov %rdi, %rcx
    jit3(0x48, 0x89, 0xf9);
   
    // mov line_number, %r8
    jit2(0x49, 0xb8);
    jit64(line_number);

    return ret;
}
void bu_free(func line) {
    jit_free(line);
}
void bu_exit(uint64_t line_number) {
    int i;
    for (i = 0; i < line_barrier_indices[line_number]; i++) {
        barrier_deregister(barriers[line_barriers[line_number][i]]);
    }
}
void bu_close(char* filename, unsigned long line_number) {
    unsigned int remaining = 0;
    while (!stack_empty(bu_loops)) {
        remaining++;
        stack_pop(bu_loops);
    }
    if (remaining) {
        #ifdef __GLIBC__
        error_at_line(EXIT_FAILURE, 0, filename, line_number, "expecting %u closing bracket(s)", remaining);
        #else
        fprintf(stderr, "%s:%lu: expecting %u closing bracket(s)", filename, line_number, remaining);
        exit(EXIT_FAILURE);
        #endif
    }
    free(bu_loops);
    // mov r8, %rdi
    jit3(0x4c, 0x89, 0xc7);
    // mov bu_exit, %rax
    jit2(0x48, 0xb8);
    uint64_t loc = (uint64_t) bu_exit;
    jit64(loc);

    // call *%rax
    jit2(0xff, 0xd0);

    // ret
    jit(0xc3);
}
void bu_plus(int should_lock) {
    if (should_lock) {
        // lock incb (%rcx)
        jit3(0xf0, 0xfe, 0x01);
    } else {
        // incb (%rcx)
        jit2(0xfe, 0x01);
    }
}
void bu_minus(int should_lock) {
    if (should_lock) {
        // lock decb (%rcx)
        jit3(0xf0, 0xfe, 0x09);
    } else {
        // lock decb (%rcx)
        jit2(0xfe, 0x09);
    }
}
void bu_add(int8_t offset, int should_lock) {
    if (should_lock) {
        // lock addb $offset, (%rcx)
        jit4(0xf0, 0x80, 0x01, offset);
    } else {
        // addb $offset, (%rcx)
        jit3(0x80, 0x01, offset);
    }
}
void bu_right(void) {
    // inc %rcx
    jit3(0x48, 0xff, 0xc1);
}
void bu_left(void) {
    // dec %rcx
    jit3(0x48, 0xff, 0xc9);
}
void bu_chg8(int8_t offset) {
    // add $offset, %rcx
    jit4(0x48, 0x83, 0xc1, offset);
}
void bu_chg32(int32_t offset) {
    // add $offset, %rcx
    jit3(0x48, 0x81, 0xc1);
    jit32(offset);
}
void bu_loop(void) {
    // loop:
    // cmpb $0, (%rcx)
    jit3(0x80, 0x39, 0x00);
    // je
    jit2(0x0f, 0x84);
    uint8_t* to_fix = jit_get();
    jit4(0,0,0,0);
    stack_push(bu_loops, to_fix);
}
void bu_end(char* filename, unsigned long line_number) {
    if (stack_empty(bu_loops)) {
        #ifdef __GLIBC__
        error_at_line(EXIT_FAILURE, 0, filename, line_number, "unexpected closing bracket");
        #else
        fprintf(stderr, "%s:%lu: unexpected closing bracket", filename, line_number);
        exit(EXIT_FAILURE);
        #endif
    }
    uint8_t* to_fix = stack_pop(bu_loops);
    // jmp to_fix - 5
    jit(0xe9);
    uint32_t rel = jit_relative((uint64_t)to_fix - 5) - 4;
    jit32(rel);
    // now fix previous je
    uint8_t* current;
    jit_set(to_fix, &current);
    rel = jit_relative((uint64_t)current) - 4;
    jit32(rel);
    jit_set(current, NULL);
}
void bu_waitzero(void) {
    // loop:
    // cmpb $0, (%rcx)
    jit3(0x80, 0x39, 0x00);
    // je end
    jit2(0x74, 0x14);
    // push %rcx
    jit(0x51);
    // push %r8
    jit2(0x41, 0x50);
    // mov sched_yield, %rax
    jit2(0x48, 0xb8);
    uint64_t loc = (uint64_t) sched_yield;
    jit64(loc);
    // call *%rax
    jit2(0xff, 0xd0);
    // pop %r8
    jit2(0x41, 0x58);
    // pop %rcx
    jit(0x59);
    // jmp loop
    jit2(0xeb, 0xe7);
    // end:
}
void bu_putchar(void) {
    int c;
    asm (
        "mov (%%rcx), %0\n\t"
        : "=r" (c)
    );
    putchar(c);
}
void bu_put(void) {
    // push rcx
    jit(0x51);
    // push r8
    jit2(0x41, 0x50);
    #ifndef __GLIBC__
    // push rcx
    jit(0x51); // 16 byte align stack
    #endif
    // mov bu_putchar, %rax
    jit2(0x48, 0xb8);
    uint64_t loc = (uint64_t) bu_putchar;
    jit64(loc);
    // call *%rax
    jit2(0xff, 0xd0);
    #ifndef __GLIBC__
    // pop rcx
    jit(0x59);
    #endif
    // pop r8
    jit2(0x41, 0x58);
    // pop rcx
    jit(0x59);
}
void bu_getchar(void) {
    uint64_t saved_rcx;
    asm (
        "mov %%rcx, %0"
        : "=m" (saved_rcx)
    );
    int c = getchar();
    asm (
        "mov %0, %%rcx"
        :
        : "m" (saved_rcx)
    );
    asm (
        "movb %0, %%al\n\t"
        "movb %%al, (%%rcx)"
        :
        : "m" (c)
        : "al"
    );
}
void bu_get(void) {
    // push rcx
    jit(0x51);
    // push r8
    jit2(0x41, 0x50);
    #ifndef __GLIBC__
    // push rcx
    jit(0x51); // 16 byte align stack
    #endif
    // mov bu_getchar, %rax
    jit2(0x48, 0xb8);
    uint64_t loc = (uint64_t) bu_getchar;
    jit64(loc);
    // call *%rax
    jit2(0xff, 0xd0);
    #ifndef __GLIBC__
    // pop rcx
    jit(0x59);
    #endif
    // pop r8
    jit2(0x41, 0x58);
    // pop rcx
    jit(0x59);
}
void bu_spawn(void) {
    void* rcx;
    int8_t offset;
    uint64_t line_number;
    asm volatile (
        "movb (%%rcx), %1\n\t"
        "mov %%rcx, %0\n\t"
        "mov %%r8, %2\n\t"
        : "=m" (rcx),
          "=r" (offset),
          "=m" (line_number)
    );
    // printf("line number %lu, offset %i\n", line_number, offset);
    bu_enter(line_number + offset);
    threadpool_spawn(pool, lines[line_number + offset], rcx);
    asm (
        "mov %0, %%rcx\n\t"
        "mov %1, %%r8\n\t"
        :
        : "m" (rcx),
          "r" (line_number)
        : "rcx", "r8"
    );
}
void bu_clone(void) {
    #ifndef __GLIBC__
    // push rcx
    jit(0x51); // stack alignment
    #endif
    // mov bu_spawn, %rax
    jit2(0x48, 0xb8);
    uint64_t loc = (uint64_t) bu_spawn;
    jit64(loc);
    // call *%rax
    jit2(0xff, 0xd0);
    #ifndef __GLIBC__
    // push rcx
    jit(0x59);
    #endif
}
void bu_block(struct barrier* block) {
    // push rcx
    jit(0x51);
    // push r8
    jit2(0x41, 0x50);
    #ifndef __GLIBC__
    // push rcx
    jit(0x51); // stack alignment
    #endif
    // mov block, %rdi
    jit2(0x48, 0xbf);
    uint64_t loc = (uint64_t) block;
    jit64(loc);
    // mov barrier_arrive, %rax
    jit2(0x48, 0xb8);
    loc = (uint64_t) barrier_arrive;
    jit64(loc);
    // call *%rax
    jit2(0xff, 0xd0);
    #ifndef __GLIBC__
    // pop rcx
    jit(0x59);
    #endif
    // pop r8
    jit2(0x41, 0x58);
    // pop rcx
    jit(0x59);
}
