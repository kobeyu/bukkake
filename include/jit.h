#ifndef JIT_H
#define JIT_H

#include "stdint.h"
typedef void* (*func) (void*);

// initializes a jit, optionally storing the old location at old
func jit_init(uint8_t** old);
// frees the function given
void jit_free(func);
// sets the jit to the provided one, optionally storing the old location at old
func jit_set(uint8_t* start, uint8_t** old);
// gets the current jit
uint8_t* jit_get(void);

// jit
void jit(uint8_t);
void jit2(uint8_t, uint8_t);
void jit3(uint8_t, uint8_t, uint8_t);
void jit4(uint8_t, uint8_t, uint8_t, uint8_t);
// align here using nops
void jit_align(void);
// jit a constant
void jit32(uint32_t);
void jit64(uint64_t);

// calculate relative address
int64_t jit_relative(uint64_t loc);

#endif
