#ifndef STACK_H
#define STACK_H

struct stack;
// create a new initialized stack
struct stack* stack_new();
// initialize a stack
void stack_init(struct stack* this);
// pop off stack
uint8_t* stack_pop(struct stack* this);
// push on stack
void stack_push(struct stack* this, uint8_t* data);
// is stack empty
int stack_empty(struct stack* this);

#endif
